/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BANK.web.restapi.activeDeposit;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.*;
import java.time.temporal.*;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.domain.deposit.dto.DepositSnapshot;

/**
 *
 * @author szczupakabra
 */
public class ActiveDeposit
   extends ResourceSupport
   implements Serializable {

    private final Long activeDepositId;

    private final Long depositId;

    private final Long accountId;

    private final LocalDateTime dateStart;

    private final Double amount;

    private final Long rate;

    private final Double calculatedAmount;

    private final Boolean isFinished;

    public ActiveDeposit(ActiveDepositSnapshot snapshot) {
        this.activeDepositId = snapshot.getActiveDepositId();
        this.depositId = snapshot.getDepositId();
        this.accountId = snapshot.getAccountId();
        this.dateStart = snapshot.getDateStart();
        this.amount = snapshot.getAmount();
        this.rate = null;
        this.calculatedAmount = this.amount;
        this.isFinished = false;
    }

    public ActiveDeposit(ActiveDepositSnapshot snapshot, DepositSnapshot depositSnapshot, Double calculatedAmount) {
        this.activeDepositId = snapshot.getActiveDepositId();
        this.depositId = snapshot.getDepositId();
        this.accountId = snapshot.getAccountId();
        this.dateStart = snapshot.getDateStart();
        this.amount = snapshot.getAmount();
        this.rate = depositSnapshot.getRate();
        this.calculatedAmount = calculatedAmount;
        this.isFinished = this.chechIfFinished(this.dateStart, depositSnapshot.getDuration(), depositSnapshot.getConstant());
    }

    public Long getActiveDepositId() {
        return activeDepositId;
    }

    public Long getDepositId() {
        return depositId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public Double getAmount() {
        return amount;
    }

    public Long getRate() {
        return rate;
    }

    public Double getCalculatedAmount() {
        return calculatedAmount;
    }

    public Boolean getIsFinished() {
        return isFinished;
    }

    private Boolean chechIfFinished(LocalDateTime dateStart, Long duration, Double constant) {
        LocalDate startDate = dateStart.toLocalDate();
        LocalDate endDate = LocalDateTime.now().toLocalDate();
        if(constant == 1) {
            return ChronoUnit.DAYS.between(startDate, endDate) > duration;
        } else if(constant == 2) {
            return ChronoUnit.MONTHS.between(startDate, endDate) > duration;
        } else {
            return ChronoUnit.YEARS.between(startDate, endDate) > duration;
        }
    }
}
