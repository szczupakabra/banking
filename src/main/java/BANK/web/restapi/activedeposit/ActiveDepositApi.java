/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package BANK.web.restapi.activeDeposit;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.time.*;
import java.time.temporal.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.account.bo.IAccountBO;
import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.finder.IAccountSnapshotFinder;
import BANK.domain.deposit.bo.IActiveDepositBO;
import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.domain.deposit.finder.IActiveDepositSnapshotFinder;
import BANK.domain.deposit.finder.IDepositSnapshotFinder;
import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.domain.transaction.bo.ITransactionBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;
import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.finder.IAccountSnapshotFinder;

/**
 *
 * @author szczupakabra
 */
@RestController
@RequestMapping("/api/activeDeposit")
public class ActiveDepositApi {
    private final IActiveDepositSnapshotFinder activeDepositSnapshotFinder;

    private final IDepositSnapshotFinder depositSnapshotFinder;
    
    private final IActiveDepositBO activeDepositBO;

    private final ITransactionBO transactionBO;

    private final IUserSnapshotFinder userSnapshotFinder;

    private final IAccountSnapshotFinder accountSnapshotFinder;

    private final IAccountBO accountBO;
    
    @Autowired
    public ActiveDepositApi(IActiveDepositSnapshotFinder activeDepositSnapshotFinder, IActiveDepositBO activeDepositBO, IUserSnapshotFinder userSnapshotFinder, IAccountBO accountBO, IAccountSnapshotFinder accountSnapshotFinder, IDepositSnapshotFinder depositSnapshotFinder, ITransactionBO transactionBO) {
        this.activeDepositSnapshotFinder = activeDepositSnapshotFinder;
        this.activeDepositBO = activeDepositBO;
        this.userSnapshotFinder = userSnapshotFinder;
        this.accountBO = accountBO;
        this.accountSnapshotFinder = accountSnapshotFinder;
        this.depositSnapshotFinder = depositSnapshotFinder;
        this.transactionBO = transactionBO;
    }

   @RequestMapping(method = RequestMethod.GET)
   public List<ActiveDeposit> myActiveDeposit(Principal principal) {

      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrueAndUserId(userSnapshot.getId()).get(0);
      List<ActiveDepositSnapshot> activeDepositSnapshots = activeDepositSnapshotFinder.findByAccountId(accountSnapshot.getId());

      List<ActiveDeposit> activeDeposits = new ArrayList<>();
      for (ActiveDepositSnapshot acs : activeDepositSnapshots) {
         DepositSnapshot depositSnapshot = depositSnapshotFinder.findByDepositId(acs.getDepositId());
         Double calculateAmount = this.calculateAmount(
            acs.getAmount(),
            acs.getDateStart(),
            LocalDateTime.now(),
            depositSnapshot.getCapitalizationPeriod(),
            depositSnapshot.getConstant(), 
            depositSnapshot.getRate(),
            depositSnapshot.getRenewable(),
            depositSnapshot.getDuration()
         );
         activeDeposits.add(new ActiveDeposit(acs, depositSnapshot, calculateAmount));
      }
      return activeDeposits;
   }

   /* 
    @RequestMapping(method = RequestMethod.GET)
    public List<ActiveDeposit> list() {
        List<ActiveDepositSnapshot> activeDepositSnapshots = activeDepositSnapshotFinder.findAll();
        return activeDepositSnapshots.stream()
           .map(ActiveDeposit::new)
           .collect(Collectors.toList());
    }
   */ 
    @RequestMapping(method = RequestMethod.POST,
       consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<ActiveDeposit> add(@RequestBody ActiveDepositNew activeDepositNew, Principal principal) {
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      DepositSnapshot depositSnapshot = depositSnapshotFinder.findByDepositId(activeDepositNew.getDepositId());
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrueAndUserId(userSnapshot.getId()).get(0);

      if(accountSnapshot.getBalance() < activeDepositNew.getAmount()
         || activeDepositNew.getAmount() < depositSnapshot.getMinCash()
         || activeDepositNew.getAmount() > depositSnapshot.getMaxCash()
         || !accountSnapshot.getCurrencyId().equals(depositSnapshot.getCurrencyId())) {
          return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }

      transactionBO.add(accountSnapshot.getIban(), "1", activeDepositNew.getAmount(), depositSnapshot.getCurrencyId());

      accountBO.boost(accountSnapshot.getId(), -activeDepositNew.getAmount());

        
        ActiveDepositSnapshot activeDepositSnapshot = activeDepositBO.add(activeDepositNew.getDepositId(), accountSnapshot.getId(), LocalDateTime.now(), activeDepositNew.getAmount());
        
        return new ResponseEntity<>(new ActiveDeposit(activeDepositSnapshot), HttpStatus.OK);
    }


   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long activeDepositId, Principal principal) {
      if (activeDepositId == null) {
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }
      ActiveDepositSnapshot activeDepositSnapshot = activeDepositSnapshotFinder.findByActiveDepositId(
         activeDepositId);
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      DepositSnapshot depositSnapshot = depositSnapshotFinder.findByDepositId(activeDepositSnapshot.getDepositId());
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrueAndUserId(userSnapshot.getId()).get(0);
      
      Double calculateAmount = this.calculateAmount(
        activeDepositSnapshot.getAmount(),
        activeDepositSnapshot.getDateStart(),
        LocalDateTime.now(),
        depositSnapshot.getCapitalizationPeriod(),
        depositSnapshot.getConstant(), 
        depositSnapshot.getRate(),
        depositSnapshot.getRenewable(),
        depositSnapshot.getDuration()
      );
      transactionBO.add("1", accountSnapshot.getIban(), calculateAmount, depositSnapshot.getCurrencyId());
      System.out.println(calculateAmount);
      accountBO.boost(accountSnapshot.getId(), calculateAmount);
      activeDepositBO.delete(activeDepositId);

      return new ResponseEntity<>(new ActiveDeposit(activeDepositSnapshot), HttpStatus.OK);
   }

   @RequestMapping(value = "stop/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity stop(@PathVariable("id") Long activeDepositId, Principal principal) {
      if (activeDepositId == null) {
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }
      ActiveDepositSnapshot activeDepositSnapshot = activeDepositSnapshotFinder.findByActiveDepositId(
         activeDepositId);
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      DepositSnapshot depositSnapshot = depositSnapshotFinder.findByDepositId(activeDepositSnapshot.getDepositId());
      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrueAndUserId(userSnapshot.getId()).get(0);
      
      Double calculateAmount = activeDepositSnapshot.getAmount();
      transactionBO.add("1", accountSnapshot.getIban(), calculateAmount, depositSnapshot.getCurrencyId());
      System.out.println(calculateAmount);
      accountBO.boost(accountSnapshot.getId(), calculateAmount);
      activeDepositBO.delete(activeDepositId);

      return new ResponseEntity<>(new ActiveDeposit(activeDepositSnapshot), HttpStatus.OK);
   }

   private Double calculateAmount(Double amount, LocalDateTime dateStart, LocalDateTime dateEnd, Long capitalizationPeriod, Double constant, Long rate, Boolean renewable, Long duration) {
        Double currentDuration = getDifference(constant,dateStart, dateEnd);
        Double periods =  Math.floor(currentDuration/capitalizationPeriod);
        Double capitalizationPerYear = getCapitalizationPerYear(constant, capitalizationPeriod);

        Double maxPeriods = Math.floor(duration*1.0/capitalizationPeriod);
        System.out.println("ML");
        System.out.println(duration);
        System.out.println(capitalizationPeriod);
        System.out.println(maxPeriods);
        if(!renewable && maxPeriods < periods ){
          System.out.println("test");
          periods = maxPeriods;
        }
        System.out.println(amount *Math.pow(1.0+rate*0.01/capitalizationPerYear,periods));
        return amount *Math.pow(1.0+rate*0.01/capitalizationPerYear,periods);
    }

    private Double getCapitalizationPerYear(Double constant, Long capitalizationPeriod) {
        if(constant == 1) {
            return 365.0/capitalizationPeriod;
        } else if(constant == 2) {
            return 12.0/capitalizationPeriod;
        }
        return 1.0/capitalizationPeriod;
    }

    private Double getDifference(Double constant, LocalDateTime dateStart, LocalDateTime dateEnd) {
        LocalDate startDate = dateStart.toLocalDate();
        LocalDate endDate = dateEnd.toLocalDate();

        if(constant == 1) {
            return 1.0 * ChronoUnit.DAYS.between(startDate, endDate);
        } else if(constant == 2) {
            return 1.0 * ChronoUnit.MONTHS.between(startDate, endDate);
        } else {
            return 1.0 * ChronoUnit.YEARS.between(startDate, endDate);
        }
    }
}
