/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.activeDeposit;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 *
 * @author szczupakabra
 */
public class ActiveDepositNew {

    @NotNull
    private Long depositId;

    @NotNull
    private Long accountId;

    @NotNull
    private LocalDateTime dateStart;

    @NotNull
    private Double amount;

    public Long getDepositId() {
        return depositId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public void setDepositId(Long depositId) {
        this.depositId = depositId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }


    //TODO add setters

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
