/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BANK.web.restapi.deposit;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.deposit.dto.DepositSnapshot;

/**
 *
 * @author szczupakabra
 */
public class Deposit
   extends ResourceSupport
   implements Serializable {

    private final Long depositId;

    private final LocalDateTime createdAt;

    private final Double constant;

    private final Long rate;

    private final Double minCash;

    private final Double maxCash;

    private final Boolean renewable;

    private final Long capitalizationPeriod;

    private final Double breakPercentage;

    private final Long currencyId;

    private final Long duration;

    public Deposit(DepositSnapshot snapshot) {
        this.depositId = snapshot.getDepositId();
        this.createdAt = snapshot.getCreatedAt();
        this.constant = snapshot.getConstant();
        this.rate = snapshot.getRate();
        this.minCash = snapshot.getMinCash();
        this.maxCash = snapshot.getMaxCash();
        this.renewable = snapshot.getRenewable();
        this.capitalizationPeriod = snapshot.getCapitalizationPeriod();
        this.breakPercentage = snapshot.getBreakPercentage();
        this.currencyId = snapshot.getCurrencyId();
        this.duration = snapshot.getDuration();
    }

    public Long getDepositId() {
        return depositId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Double getConstant() {
        return constant;
    }

    public Long getRate() {
        return rate;
    }

    public Double getMinCash() {
        return minCash;
    }

    public Double getMaxCash() {
        return maxCash;
    }

    public Boolean getRenewable() {
        return renewable;
    }

    public Long getCapitalizationPeriod() {
        return capitalizationPeriod;
    }

    public Double getBreakPercentage() {
        return breakPercentage;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public Long getDuration() {
        return duration;
    }
}
