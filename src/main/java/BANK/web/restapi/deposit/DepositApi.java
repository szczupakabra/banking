/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package BANK.web.restapi.deposit;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.deposit.bo.IDepositBO;
import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.domain.deposit.finder.IDepositSnapshotFinder;
/**
 *
 * @author szczupakabra
 */
@RestController
@RequestMapping("/api/deposit")
public class DepositApi {
    private final IDepositSnapshotFinder depositSnapshotFinder;
    
    private final IDepositBO depositBO;
    
    @Autowired
    public DepositApi(IDepositSnapshotFinder depositSnapshotFinder, IDepositBO depositBO) {
        this.depositSnapshotFinder = depositSnapshotFinder;
        this.depositBO = depositBO;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public List<Deposit> list() {
        List<DepositSnapshot> depositSnapshots = depositSnapshotFinder.findAll();
        
        return depositSnapshots.stream()
           .map(Deposit::new)
           .collect(Collectors.toList());
    }
    
    @RequestMapping(method = RequestMethod.POST,
       consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Deposit> add(@RequestBody DepositNew depositNew) {
        
        DepositSnapshot depositSnapshot = depositBO.add(depositNew.getConstant(), depositNew.getRate(), depositNew.getMinCash(),
           depositNew.getMaxCash(), depositNew.isRenewable(), depositNew.getCapitalizationPeriod(),depositNew.getBreakPercentage(),depositNew.getCurrencyId(), depositNew.getDuration());
        
        return new ResponseEntity<>(new Deposit(depositSnapshot), HttpStatus.OK);
    }

   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long depositId) {
      DepositSnapshot depositSnapshot = depositSnapshotFinder.findByDepositId(
         depositId);
      
      if (depositId != null) {
         depositBO.delete(depositId);
      }

      return new ResponseEntity<>(new Deposit(depositSnapshot), HttpStatus.OK);
   }
}
