/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.deposit;
import javax.validation.constraints.NotNull;

/**
 *
 * @author szczupakabra
 */
public class DepositNew {
    
	@NotNull
	private Double constant;

	@NotNull
	private Long rate;

	@NotNull
	private Double minCash;

	@NotNull
	private Double maxCash;

	@NotNull
	private boolean renewable;

	@NotNull
	private Long capitalizationPeriod;

	@NotNull
	private Double breakPercentage;

    @NotNull
    private Long currencyId;

    @NotNull
    private Long duration;

    public Double getConstant() {
        return constant;
    }

    public void setConstant(Double constant) {
        this.constant = constant;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Double getMinCash() {
        return minCash;
    }

    public void setMinCash(Double minCash) {
        this.minCash = minCash;
    }

    public Double getMaxCash() {
        return maxCash;
    }

    public void setMaxCash(Double maxCash) {
        this.maxCash = maxCash;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public Long getCapitalizationPeriod() {
        return capitalizationPeriod;
    }

    public void setCapitalizationPeriod(Long capitalizationPeriod) {
        this.capitalizationPeriod = capitalizationPeriod;
    }

    public Double getBreakPercentage() {
        return breakPercentage;
    }

    public void setBreakPercentage(Double breakPercentage) {
        this.breakPercentage = breakPercentage;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }
    
}
