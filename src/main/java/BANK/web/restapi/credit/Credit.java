package BANK.web.restapi.credit;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.credit.dto.CreditSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */
public class Credit
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long creditId;

   private final LocalDateTime createdAt;

   private final Long provision;

   private final Long interest;

   private final Long installment;

   private final Double contribution;

   private final Double minValue;

   private final Double maxValue;

   private final boolean changingRate;

   private final boolean active;

   private final boolean insurance;

   private final String type;

   private final Long currencyId;

   private final Long granted;

   public Credit(CreditSnapshot creditSnapshot) {
      this.creditId = creditSnapshot.getId();
      this.createdAt = creditSnapshot.getCreatedAt();
      this.provision = creditSnapshot.getProvision();
      this.installment = creditSnapshot.getInstallment();
      this.interest = creditSnapshot.getInterest();
      this.active = creditSnapshot.isActive();
      this.insurance = creditSnapshot.isInsurance();
      this.contribution = creditSnapshot.getContribution();
      this.changingRate = creditSnapshot.isChangingRate();
      this.maxValue = creditSnapshot.getMaxValue();
      this.minValue = creditSnapshot.getMinValue();
      this.type = creditSnapshot.getType();
      this.currencyId = creditSnapshot.getCurrencyId();
      this.granted = 0L;
   }

   public Credit(CreditSnapshot creditSnapshot,Long granted) {
      this.creditId = creditSnapshot.getId();
      this.createdAt = creditSnapshot.getCreatedAt();
      this.provision = creditSnapshot.getProvision();
      this.installment = creditSnapshot.getInstallment();
      this.interest = creditSnapshot.getInterest();
      this.active = creditSnapshot.isActive();
      this.insurance = creditSnapshot.isInsurance();
      this.contribution = creditSnapshot.getContribution();
      this.changingRate = creditSnapshot.isChangingRate();
      this.maxValue = creditSnapshot.getMaxValue();
      this.minValue = creditSnapshot.getMinValue();
      this.type = creditSnapshot.getType();
      this.currencyId = creditSnapshot.getCurrencyId();
      this.granted = granted;
   }

   public Long getCreditId() {
      return creditId;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public Long getProvision() {
      return provision;
   }

   public Long getInterest() {
      return interest;
   }

   public Long getInstallment() {
      return installment;
   }

   public Double getContribution() {
      return contribution;
   }

   public boolean isChangingRate() {
      return changingRate;
   }

   public boolean isActive() {
      return active;
   }

   public boolean isInsurance() {
      return insurance;
   }

   public Double getMinValue() {
      return minValue;
   }

   public Double getMaxValue() {
      return maxValue;
   }

   public String getType() {
      return type;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public Long getGranted() {
      return granted;
   }
   
   
}
