/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.currency;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Mateusz
 */
public class CurrencyEdit {
   
   @NotNull
   private Long currencyId;
   
   @NotNull
   private String symbol;

   public Long getCurrencyId() {
      return currencyId;
   }

   public void setCurrencyId(Long currencyId) {
      this.currencyId = currencyId;
   }

   public String getSymbol() {
      return symbol;
   }

   public void setSymbol(String symbol) {
      this.symbol = symbol;
   }
   
}
