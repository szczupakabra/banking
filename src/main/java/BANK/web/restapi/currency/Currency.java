package BANK.web.restapi.currency;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.currency.dto.CurrencySnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class Currency
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long currencyId;

   private final String symbol;
   
   public Currency(CurrencySnapshot currencySnapshot) {
      this.currencyId = currencySnapshot.getId();
      this.symbol = currencySnapshot.getSymbol();
   }
   
   public Long getCurrencyId() {
      return currencyId;
   }

   public String getSymbol() {
      return symbol;
   }
}
