package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ActiveCreditController {

  @RequestMapping("/activecredit/list")
   public String list() {
      return "activecredit/list";
   }

   @RequestMapping("/activecredit/common")
   public String common() {
      return "activecredit/common";
   }

   @RequestMapping("/activecredit/details")
   public String detail() {
      return "activecredit/details";
   }
   
   @RequestMapping("/activecredit/add")
   public String add() {
      return "activecredit/add";
   }
   
   @RequestMapping("/activecredit/edit")
   public String edit() {
      return "activecredit/edit";
   }

}
