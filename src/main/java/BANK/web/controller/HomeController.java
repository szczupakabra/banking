package BANK.web.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import BANK.domain.user.bo.IUserBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;
import BANK.sharedkernel.constant.Profiles;

@Controller
public class HomeController {

   private final Environment environment;
   private final IUserSnapshotFinder userSnapshotFinder;
   private final IUserBO userBO;

   @Autowired
   public HomeController(Environment environment,
      IUserSnapshotFinder userSnapshotFinder,
      IUserBO userBO) {
      this.environment = environment;
      this.userSnapshotFinder = userSnapshotFinder;
      this.userBO = userBO ; 
   }

   @RequestMapping("/")
   public ModelAndView root(ModelAndView modelAndView, Principal principal) {
      modelAndView.setViewName("index");

      List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());

      addIfContains(modelAndView, activeProfiles, Profiles.BASIC_AUTHENTICATION);
      addIfContains(modelAndView, activeProfiles, Profiles.DEVELOPMENT);

      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      modelAndView.addObject("GUID", userSnapshot.getGuid().toString());

      return modelAndView;
   }

//   @RequestMapping(value = "/logout",
//      method = RequestMethod.GET)
//   public String logout(Principal principal) {
//      UserSnapshot emp = userSnapshotFinder.findByUsername(principal.getName());
//      String uuid = UUID.randomUUID().toString();
//      userBO.newPassword(emp.getId(), uuid);
//
//     
//      return "/logout";
//   }
   
   @RequestMapping("/login")
   public ModelAndView login(ModelAndView modelAndView) {
      modelAndView.setViewName("login");

      List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());

      addIfContains(modelAndView, activeProfiles, Profiles.BASIC_AUTHENTICATION);
      addIfContains(modelAndView, activeProfiles, Profiles.DEVELOPMENT);

      return modelAndView;
   }

   @RequestMapping(value = "/password",
      method = RequestMethod.GET)
   public ModelAndView password(ModelAndView modelAndView) {
      modelAndView.setViewName("password");
      return modelAndView;
   }
   
      @RequestMapping(value = "/password",
      method = RequestMethod.POST)
   public ModelAndView passwordP(ModelAndView modelAndView,@RequestParam("username") String username) {
      modelAndView.setViewName("password");
      UserSnapshot emp = userSnapshotFinder.findByUsername(username);
      if (!(emp == null)) {
         String uuid = UUID.randomUUID().toString();
         userBO.newPassword(emp.getId(), uuid);
         modelAndView.addObject("yourPassword", "Twoje nowe hasło to: \r\n " + uuid);
      }
      
      return modelAndView;
   }
   
   private void addIfContains(ModelAndView modelAndView, List<String> profiles, String profile) {
      if (profiles.contains(profile)) {
         modelAndView.addObject(profile, true);
      }
   }

}
