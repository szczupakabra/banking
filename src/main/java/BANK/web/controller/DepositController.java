
package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DepositController {

  @RequestMapping("/deposit/list")
   public String list() {
      return "deposit/list";
   }
/*
   @RequestMapping("/credit/common")
   public String common() {
      return "credit/common";
   }

   @RequestMapping("/credit/details")
   public String detail() {
      return "credit/details";
   }
  */ 
   @RequestMapping("/deposit/add")
   public String add() {
      return "deposit/add";
   }
  /* 
   @RequestMapping("/credit/edit")
   public String edit() {
      return "credit/edit";
   }
*/
}
