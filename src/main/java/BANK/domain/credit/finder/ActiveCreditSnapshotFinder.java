package BANK.domain.credit.finder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.entity.ActiveCredit;
import BANK.domain.credit.repo.IActiveCreditRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class ActiveCreditSnapshotFinder
   implements IActiveCreditSnapshotFinder {

   private final IActiveCreditRepository creditRepository;

   @Autowired
   public ActiveCreditSnapshotFinder(IActiveCreditRepository creditRepository) {
      this.creditRepository = creditRepository;
   }

   private List<ActiveCreditSnapshot> convert(List<ActiveCredit> credits) {
      return credits.stream()
         .map(ActiveCredit::toSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public ActiveCreditSnapshot findById(Long id) {
      ActiveCredit credit = creditRepository.findOne(id);
      return credit == null ? null : credit.toSnapshot();
   }

   @Override
   public List<ActiveCreditSnapshot> findByUserId(Long id) {
      List<ActiveCredit> credits = creditRepository.findByUserId(id);

      return convert(credits);
   }

   @Override
   public List<ActiveCreditSnapshot> findByCreditId(Long id) {
      List<ActiveCredit> credits = creditRepository.findByCreditId(id);

      return convert(credits);
   }

   @Override
   public List<ActiveCreditSnapshot> findAll() {
      List<ActiveCredit> credits = creditRepository.findAll();

      return convert(credits);
   }

   @Override
   public List<ActiveCreditSnapshot> findAll(Set<Long> ids) {
      List<ActiveCredit> credits = creditRepository.findAll(ids);

      return convert(credits);
   }

   @Override
   public List<ActiveCreditSnapshot> findActive() {
      return convert(creditRepository.findByActiveTrue());
   }

}
