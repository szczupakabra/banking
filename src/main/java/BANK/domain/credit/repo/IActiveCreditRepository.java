package BANK.domain.credit.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import BANK.domain.credit.entity.ActiveCredit;
import BANK.domain.credit.entity.Credit;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IActiveCreditRepository
   extends JpaRepository<ActiveCredit, Long> {

   List<ActiveCredit> findByActiveTrue();
   
   List<ActiveCredit> findByUserId(Long userId);
   
   List<ActiveCredit> findByCreditId(Long creditId);
}
