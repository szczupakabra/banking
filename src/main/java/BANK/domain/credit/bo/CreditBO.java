package BANK.domain.credit.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.entity.Credit;
import BANK.domain.credit.repo.ICreditRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class CreditBO
   implements ICreditBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(CreditBO.class);
   private final ICreditRepository creditRepository;

   
   @Autowired
   public CreditBO(ICreditRepository creditRepository) {
      this.creditRepository = creditRepository;
   }

   @Override
   public CreditSnapshot add(Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
      boolean active, boolean insurance,Double minValue, Double maxValue, String type, Long currencyId) {
      Credit credit = new Credit(provision,interest,installment,contribution,changingRate, active,insurance,minValue, maxValue, type,currencyId);

      credit = creditRepository.save(credit);
      
      CreditSnapshot creditSnapshot = credit.toSnapshot();

      LOGGER.info("Add Credit <{}>", creditSnapshot.getId());

      return creditSnapshot;
   }

   @Override
   public CreditSnapshot edit(Long id, Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
       boolean insurance,Double minValue, Double maxValue, String type, Long currencyId) {

      Credit credit = creditRepository.findOne(id);

      credit.editCredit(provision,interest,installment,contribution,changingRate, insurance,minValue, maxValue,type, currencyId);

      creditRepository.save(credit);

      CreditSnapshot creditSnapshot = credit.toSnapshot();

      LOGGER.info("Edit Credit <{}>", creditSnapshot.getId());
      return creditSnapshot;
   }

   @Override
   public void delete(Long creditId) {
      Credit credit = creditRepository.findOne(creditId);
      creditRepository.delete(credit);

      LOGGER.info("Credit <{}> marked as deleted", creditId);
   }
   
   @Override
   public void finish(Long creditId) {
      Credit credit = creditRepository.findOne(creditId);
      credit.finish();
      creditRepository.save(credit);

      CreditSnapshot creditSnapshot = credit.toSnapshot();
      
      LOGGER.info("Credit <{}>  marked as finished",   creditId);
   }
}
