/**
 * This domain is concentrated on credit management. It allows to access credit data through REST API and manage
 * them in the database.
 */
package BANK.domain.credit;
