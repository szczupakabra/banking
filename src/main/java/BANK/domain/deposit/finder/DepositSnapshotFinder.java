/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.finder;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.domain.deposit.repo.IDepositRepository;
import BANK.domain.deposit.entity.Deposit;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author szczupakabra
 */
@Finder
public class DepositSnapshotFinder
   implements IDepositSnapshotFinder {

	private final IDepositRepository depositRepository;

	@Autowired
	public DepositSnapshotFinder(IDepositRepository depositRepository) {
		this.depositRepository = depositRepository;
	}

	@Override
	public List<DepositSnapshot> findAll() {
		List<Deposit> deposits = depositRepository.findAll();

		return deposits.stream()
		   .map(Deposit::toSnapshot)
		   .collect(Collectors.toList());
	}

    @Override
    public DepositSnapshot findByDepositId(Long depositId) {
        Deposit deposit = depositRepository.findOne(depositId);
        return deposit == null ? null : deposit.toSnapshot();
    }

}
