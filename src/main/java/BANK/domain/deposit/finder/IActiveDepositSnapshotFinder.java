/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.finder;

import java.util.List;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;

/**
 *
 * @author szczupakabraa
 */
public interface IActiveDepositSnapshotFinder {
    List<ActiveDepositSnapshot> findAll();
    ActiveDepositSnapshot findByActiveDepositId(Long activeDepositId);
    List<ActiveDepositSnapshot> findByAccountId(Long accountId);
    
}