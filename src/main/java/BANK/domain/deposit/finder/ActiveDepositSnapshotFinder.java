/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.activeActiveDeposit.finder;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.domain.deposit.finder.IActiveDepositSnapshotFinder;
import BANK.domain.deposit.repo.IActiveDepositRepository;
import BANK.domain.deposit.entity.ActiveDeposit;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author szczupakabra
 */
@Finder
public class ActiveDepositSnapshotFinder
   implements IActiveDepositSnapshotFinder {

    private final IActiveDepositRepository activeActiveDepositRepository;

    @Autowired
    public ActiveDepositSnapshotFinder(IActiveDepositRepository activeActiveDepositRepository) {
        this.activeActiveDepositRepository = activeActiveDepositRepository;
    }

    @Override
    public List<ActiveDepositSnapshot> findAll() {
        List<ActiveDeposit> activeActiveDeposits = activeActiveDepositRepository.findAll();

        return activeActiveDeposits.stream()
           .map(ActiveDeposit::toSnapshot)
           .collect(Collectors.toList());
    }

    @Override
    public ActiveDepositSnapshot findByActiveDepositId(Long activeActiveDepositId) {
        ActiveDeposit activeActiveDeposit = activeActiveDepositRepository.findOne(activeActiveDepositId);
        return activeActiveDeposit == null ? null : activeActiveDeposit.toSnapshot();
    }

    @Override
    public List<ActiveDepositSnapshot> findByAccountId(Long accountId) {
        List<ActiveDeposit> activeDeposits = activeActiveDepositRepository.findByAccountId(accountId);

        return activeDeposits.stream()
           .map(ActiveDeposit::toSnapshot)
           .collect(Collectors.toList());
    }
}
