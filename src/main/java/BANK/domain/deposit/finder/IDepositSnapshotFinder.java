/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.finder;

import java.util.List;

import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.deposit.dto.DepositSnapshot;

/**
 *
 * @author szczupakabraa
 */
public interface IDepositSnapshotFinder {
	List<DepositSnapshot> findAll();
    DepositSnapshot findByDepositId(Long depositId);
	
}
