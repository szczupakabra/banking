/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.dto;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author szczupakabra
 */
public class DepositSnapshot {

	private final Long depositId;

	private final LocalDateTime createdAt;

	private final Double constant;

	private final Long rate;

	private final Double minCash;

	private final Double maxCash;

	private final Boolean renewable;

	private final Long capitalizationPeriod;

	private final Double breakPercentage;

    private final Long currencyId;

    private final Long duration;

	public DepositSnapshot(Long depositId, LocalDateTime createdAt, Double constant, Long rate, Double minCash,
	   Double maxCash, Boolean renewable, Long capitalizationPeriod, Double breakPercentage, Long currencyId, Long duration) {
		this.depositId = depositId;
		this.createdAt = createdAt;
		this.constant = constant;
		this.rate = rate;
		this.minCash = minCash;
		this.maxCash = maxCash;
		this.renewable = renewable;
		this.capitalizationPeriod = capitalizationPeriod;
		this.breakPercentage = breakPercentage;
        this.currencyId = currencyId;
        this.duration = duration;
	}

	public Long getDepositId() {
		return depositId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public Double getConstant() {
		return constant;
	}

	public Long getRate() {
		return rate;
	}

	public Double getMinCash() {
		return minCash;
	}

	public Double getMaxCash() {
		return maxCash;
	}

	public Boolean getRenewable() {
		return renewable;
	}

	public Long getCapitalizationPeriod() {
		return capitalizationPeriod;
	}

	public Double getBreakPercentage() {
		return breakPercentage;
	}

    public Long getCurrencyId() {
        return currencyId;
    }

    public Long getDuration() {
		return duration;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 97 * hash + Objects.hashCode(this.depositId);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DepositSnapshot other = (DepositSnapshot) obj;
		if (!Objects.equals(this.depositId, other.depositId)) {
			return false;
		}
		return true;
	}

}
