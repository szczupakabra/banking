/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.dto;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author szczupakabra
 */
public class ActiveDepositSnapshot {

	private final Long activeDepositId;

	private final Long depositId;

	private final Long accountId;

	private final LocalDateTime dateStart;

	private final Double amount;

	public ActiveDepositSnapshot(Long activeDepositId, Long depositId, Long accountId, LocalDateTime dateStart, Double amount) {
		this.activeDepositId = activeDepositId;
		this.depositId = depositId;
		this.accountId = accountId;
		this.dateStart = dateStart;
		this.amount = amount;
	}

	public Long getActiveDepositId() {
		return activeDepositId;
	}

	public Long getDepositId() {
		return depositId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public LocalDateTime getDateStart() {
		return dateStart;
	}

	public Double getAmount() {
		return amount;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 53 * hash + Objects.hashCode(this.activeDepositId);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ActiveDepositSnapshot other = (ActiveDepositSnapshot) obj;
		if (!Objects.equals(this.activeDepositId, other.activeDepositId)) {
			return false;
		}
		return true;
	}
	
}
