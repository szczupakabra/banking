/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.repo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.domain.deposit.entity.ActiveDeposit;

/**
 *
 * @author szczupakabra
 */
public interface IActiveDepositRepository
   extends JpaRepository<ActiveDeposit, Long> {
	
	ActiveDeposit findByActiveDepositId(Long activeDepositId);
	
	List<ActiveDeposit> findByAccountId(Long accountId);
}
