/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.repo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import BANK.domain.deposit.entity.Deposit;

/**
 *
 * @author szczupakabra
 */
public interface IDepositRepository
   extends JpaRepository<Deposit, Long> {

	List<Deposit> findAll();

}
