/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.sharedkernel.exception.EntityInStateNewException;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;

/**
 *
 * @author szczupakabra
 */
@Entity
public class ActiveDeposit
   implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long activeDepositId;

	@NotNull
	private Long depositId;

	@NotNull
	private Long accountId;

	@NotNull
	@Convert(converter = LocalDateTimePersistenceConverter.class)
	private LocalDateTime dateStart;

	@NotNull
	private Double amount;

	protected ActiveDeposit() {
	}

	public ActiveDeposit(Long depositId, Long accountId, LocalDateTime dateStart,  Double amount) {
		this.depositId = depositId;
		this.accountId = accountId;
		this.dateStart = dateStart;
		this.amount = amount;
	}

	public ActiveDepositSnapshot toSnapshot() {
		if (depositId == null) {
			throw new EntityInStateNewException();
		}

		return new ActiveDepositSnapshot(activeDepositId, depositId, accountId, dateStart, amount);
	}

}
