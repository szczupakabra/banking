/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author szczupakabra
 */
@Entity
public class Deposit
   implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long depositId;

	@NotNull
	@Convert(converter = LocalDateTimePersistenceConverter.class)
	private LocalDateTime createdAt;

	@NotNull
	@Column(name = "const")
	private Double constant;

	@NotNull
	private Long rate;

	@NotNull
	private Double minCash;

	@NotNull
	private Double maxCash;

	@NotNull
	private Boolean renewable;

	@NotNull
	private Long capitalizationPeriod;

	@NotNull
	private Double breakPercentage;

    @NotNull
    private Long currencyId;

    @NotNull
    private Long duration;

	protected Deposit() {
	}

	public DepositSnapshot toSnapshot() {
		if (depositId == null) {
			throw new EntityInStateNewException();
		}

		return new DepositSnapshot(depositId, createdAt, constant, rate, minCash, maxCash, renewable,
		   capitalizationPeriod, breakPercentage, currencyId, duration);
	}

	public Deposit(Double constant, Long rate, Double minCash, Double maxCash,
	   Boolean renewable, Long capitalizationPeriod, Double breakPercentage, Long currencyId, Long duration) {
		this.createdAt = LocalDateTime.now();
		this.constant = constant;
		this.rate = rate;
		this.minCash = minCash;
		this.maxCash = maxCash;
		this.renewable = renewable;
		this.capitalizationPeriod = capitalizationPeriod;
		this.breakPercentage = breakPercentage;
        this.currencyId = currencyId;
        this.duration = duration;
	}

	public void editDeposit(Double constant, Long rate, Double minCash, Double maxCash,
	   Boolean renewable, Long capitalizationPeriod, Double breakPercentage) {
		this.constant = constant;
		this.rate = rate;
		this.minCash = minCash;
		this.maxCash = maxCash;
		this.renewable = renewable;
		this.capitalizationPeriod = capitalizationPeriod;
		this.breakPercentage = breakPercentage;

	}

}
