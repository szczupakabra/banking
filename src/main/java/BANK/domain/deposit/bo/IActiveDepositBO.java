/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.bo;

import java.time.LocalDateTime;
import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author szczupakabra
 */
public interface IActiveDepositBO {

    ActiveDepositSnapshot add(Long depositId, Long accountId, LocalDateTime dateStart, Double amount);
    void delete(Long activeDepositId);
    
}
