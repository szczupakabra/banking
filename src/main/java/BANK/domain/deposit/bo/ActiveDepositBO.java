/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BANK.domain.deposit.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.LocalDateTime;

import BANK.domain.deposit.dto.ActiveDepositSnapshot;
import BANK.domain.deposit.entity.ActiveDeposit;
import BANK.domain.deposit.repo.IActiveDepositRepository;
import BANK.domain.deposit.bo.IActiveDepositBO;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author szczupakabra
 */
@BussinesObject
public class ActiveDepositBO
   implements IActiveDepositBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveDepositBO.class);
    private final IActiveDepositRepository activeDepositRepository;

    @Autowired
    public ActiveDepositBO(IActiveDepositRepository activeDepositRepository) {
        this.activeDepositRepository = activeDepositRepository;
    }

    @Override
    public ActiveDepositSnapshot add(Long depositId, Long accountId, LocalDateTime dateStart, Double amount) {
        ActiveDeposit activeDeposit = new ActiveDeposit(depositId, accountId, dateStart, amount);

        activeDeposit = activeDepositRepository.save(activeDeposit);

        ActiveDepositSnapshot activeDepositSnapshot = activeDeposit.toSnapshot();

        return activeDepositSnapshot;
    }

    @Override
    public void delete(Long activeDepositId) {
        ActiveDeposit activeDeposit = activeDepositRepository.findOne(activeDepositId);
        activeDepositRepository.delete(activeDeposit);
    }
}
