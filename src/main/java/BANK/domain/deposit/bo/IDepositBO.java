/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.domain.deposit.bo;

import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author szczupakabra
 */
public interface IDepositBO {

	DepositSnapshot add(Double constant, Long rate, Double minCash, Double maxCash, Boolean renewable, Long capitalizationPeriod, Double breakPercentage, Long currencyId, Long duration);
    void delete(Long depositId);
	
}
