/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BANK.domain.deposit.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import BANK.domain.deposit.dto.DepositSnapshot;
import BANK.domain.deposit.entity.Deposit;
import BANK.domain.deposit.repo.IDepositRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author szczupakabra
 */
@BussinesObject
public class DepositBO
   implements IDepositBO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepositBO.class);
	private final IDepositRepository depositRepository;

	@Autowired
	public DepositBO(IDepositRepository depositRepository) {
		this.depositRepository = depositRepository;
	}

	@Override
	public DepositSnapshot add(Double constant, Long rate, Double minCash, Double maxCash, Boolean renewable,
	   Long capitalizationPeriod, Double breakPercentage, Long currencyId, Long duration) {
		Deposit deposit = new Deposit(constant, rate, minCash, maxCash, renewable, capitalizationPeriod, breakPercentage, currencyId, duration);

		deposit = depositRepository.save(deposit);

		DepositSnapshot depositSnapshot = deposit.toSnapshot();

		return depositSnapshot;
	}

    @Override
    public void delete(Long depositId) {
        Deposit deposit = depositRepository.findOne(depositId);
        depositRepository.delete(deposit);
    }
}
