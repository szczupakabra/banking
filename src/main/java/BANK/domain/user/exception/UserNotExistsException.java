package BANK.domain.user.exception;

public class UserNotExistsException
   extends RuntimeException {

   private static final long serialVersionUID = -6409333454995020070L;

   /**
    * Constructs an instance of <code>UserNotExists</code> with the specified detail message.
    *
    * @param message the detail message.
    */
   public UserNotExistsException(String message) {
      super(message);
   }

}
