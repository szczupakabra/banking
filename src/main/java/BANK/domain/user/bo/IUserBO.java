package BANK.domain.user.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.user.dto.UserSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IUserBO {

   UserSnapshot add(UUID guid, String firstname, String lastname,
      String title, String username,
      LocalDateTime createdAt, String mail, String role, String password);

   void block(Long userId);

   UserSnapshot edit(long id, UUID guid, String name, String lastname,
      String title, String username, String mail);

   UserSnapshot unlock(Long userId);

   void delete(Long userId);
   
   void newPassword(Long userId,String password);

}
