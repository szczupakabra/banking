package BANK.domain.installment.finder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.domain.installment.entity.Installment;
import BANK.domain.installment.repo.IInstallmentRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class InstallmentSnapshotFinder
   implements IInstallmentSnapshotFinder {

    private final IInstallmentRepository installmentRepository;

   @Autowired
   public InstallmentSnapshotFinder(IInstallmentRepository installmentRepository) {
      this.installmentRepository = installmentRepository;
   }

   private List<InstallmentSnapshot> convert(List<Installment> installments) {
      return installments.stream()
         .map(Installment::toSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public InstallmentSnapshot findById(Long id) {
      Installment installment = installmentRepository.findOne(id);
      return installment == null ? null : installment.toSnapshot();
   }
   
   @Override
   public List<InstallmentSnapshot> findAll() {
      List<Installment> installments = installmentRepository.findAll();

      return convert(installments);
   }

   @Override
   public List<InstallmentSnapshot> findAll(Set<Long> ids) {
      List<Installment> installments = installmentRepository.findAll(ids);

      return convert(installments);
   }

   @Override
   public List<InstallmentSnapshot> findActive() {
      return convert(installmentRepository.findByActiveTrue());
   }

   @Override
   public List<InstallmentSnapshot> findByActiveCreditId(Long creditId) {
      List<Installment> installments = installmentRepository.findByActiveCreditId(creditId);

      return convert(installments);
   }

   @Override
   public List<InstallmentSnapshot> findByActiveTrueAndActiveCreditId(Long creditId) {
      List<Installment> installments = installmentRepository.findByActiveTrueAndActiveCreditId(creditId);

      return convert(installments);
   }
   
}
