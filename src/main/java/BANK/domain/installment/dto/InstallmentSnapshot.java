package BANK.domain.installment.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class InstallmentSnapshot {

   private final Long id;
   
   private final Long activeCreditId;

   private final LocalDateTime createdAt;
   
   private final LocalDateTime paiedAt;
   
   private final LocalDateTime finishDate;
   
   private final long version;
   
   private final Double amount;
   
   private final boolean active;


   public InstallmentSnapshot(Long id, LocalDateTime createdAt,  LocalDateTime paiedAt, Double amount,
         long version, boolean active,LocalDateTime finishDate,Long activeCreditId) {
      this.id = id;
      this.createdAt = createdAt;
      this.version = version;
      this.active = active;
      this.paiedAt = paiedAt;
      this.amount = amount;
      this.finishDate = finishDate;
      this.activeCreditId = activeCreditId;
   }

   public Long getId() {
      return id;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public LocalDateTime getPaiedAt() {
      return paiedAt;
   }

   public long getVersion() {
      return version;
   }

   public Double getAmount() {
      return amount;
   }

   public boolean isActive() {
      return active;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public Long getActiveCreditId() {
      return activeCreditId;
   }

   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof InstallmentSnapshot)) {
         return false;
      }
      InstallmentSnapshot emp = (InstallmentSnapshot) obj;
      return this.getId().equals(emp.getId());
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.id);
      return hash;
   }

}
