package BANK.domain.installment.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import BANK.domain.installment.entity.Installment;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IInstallmentRepository
   extends JpaRepository<Installment, Long> {

   List<Installment> findByActiveTrue();
   
   List<Installment> findByActiveTrueAndActiveCreditId(Long activeCreditId);
   
   List<Installment> findByActiveCreditId(Long activeCreditId);
}
