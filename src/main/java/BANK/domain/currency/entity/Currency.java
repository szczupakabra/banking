package BANK.domain.currency.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.currency.dto.CurrencySnapshot;
import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Currency
   implements Serializable {

   private static final long serialVersionUID = -1355310491554028529L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotEmpty
   @Size(min = 1,
      max = 5)
   @Column(nullable = false,
      unique = true)
   private String symbol;


   protected Currency() {
   }

   public Currency(String symbol) {
      this.symbol = symbol;
   }

   public void editCurrency(String symbol) {
      this.symbol = symbol;
   }

   public CurrencySnapshot toCurrencySnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }
      return new CurrencySnapshot(id, symbol);
   }

}
