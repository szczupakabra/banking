package BANK.domain.currency.finder;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import BANK.domain.currency.dto.CurrencySnapshot;
import BANK.domain.currency.entity.Currency;
import BANK.domain.currency.repo.ICurrencyRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class CurrencySnapshotFinder
   implements ICurrencySnapshotFinder {

   private final ICurrencyRepository currencyRepository;

   @Autowired
   public CurrencySnapshotFinder(ICurrencyRepository currencyRepository) {
      this.currencyRepository = currencyRepository;
   }

   private List<CurrencySnapshot> convert(List<Currency> currencys) {
      return currencys.stream()
         .map(Currency::toCurrencySnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public CurrencySnapshot findById(Long id) {
      Currency currency = currencyRepository.findOne(id);
      return currency == null ? null : currency.toCurrencySnapshot();
   }

   @Override
   public List<CurrencySnapshot> findAll() {
      List<Currency> currencys = currencyRepository.findAll();

      return convert(currencys);
   }

   @Override
   public CurrencySnapshot findBySymbol(String symbol) {
      List<Currency> currencys = currencyRepository.findBySymbol(symbol);

      return currencys.isEmpty() ? null : currencys.get(0).toCurrencySnapshot();
   }

   @Override
   public List<CurrencySnapshot> findAll(Set<Long> ids) {
      List<Currency> currencys = currencyRepository.findAll(ids);

      return convert(currencys);
   }

}
