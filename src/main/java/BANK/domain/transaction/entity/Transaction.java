package BANK.domain.transaction.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.transaction.dto.TransactionSnapshot;
import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Transaction
   implements Serializable {

   private static final long serialVersionUID = -1355310491554028529L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotEmpty
   @Size(min = 1,
      max = 50)
   @Column(nullable = false,
      unique = true)
   private String fromIban;

   @NotEmpty
   @Size(min = 1,
      max = 50)
   @Column(nullable = false,
      unique = true)
   private String toIban;
   
   @NotNull
   private Long currencyId;
   
   @NotNull
   private Double value;
   
   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;

   protected Transaction() {
   }

   public Transaction(String fromIban, String toIban, Double value, Long currencyId) {
      this.createdAt = LocalDateTime.now();
      this.toIban = toIban;
      this.fromIban = fromIban;
      this.currencyId = currencyId;
      this.value = value;
   }


   public TransactionSnapshot toTransactionSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }
      return new TransactionSnapshot(id,fromIban, toIban, value,createdAt,currencyId);
   }

}
