package BANK.config.development;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.user.bo.IUserBO;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class UserInitializer
   implements IUserInitializer {

  @Autowired
   private IUserBO userBO;

   @Transactional
   @Override
   public void initalizer() {

      userBO.add(UUID.randomUUID(), "Mateusz", "Głąbicki",
         "Worker", "mateusz.glabicki",
         LocalDateTime.now(), "mateusz.glabicki@bank.pl","ROLE_WORKER","password");
      
      userBO.add(UUID.randomUUID(), "Mateusz", "Łędzewicz",
         "Admin", "mateusz.ledzewicz",
         LocalDateTime.now(), "mateusz.łędzewicz@bank.pl","ROLE_ADMIN","password");
      
      userBO.add(UUID.randomUUID(), "Mariusz", "Kłysiński",
         "User", "mariusz.klysinski",
         LocalDateTime.now(), "mariusz.klysinski@bank.pl","ROLE_USER","password");
      
   }

}
