(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Installment', Installment);

   Installment.$inject = ['$resource', 'dateResolveUtil'];

   function Installment($resource, dateResolveUtil) {
      return $resource('/api/installment/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         pay : {
            method:'PUT',
            url: '/api/installment/pay/:id',
            params:{id : '@id'}
         },
         creditInstallment : {
            method:'GET',
            url: '/api/installment/credit/:id',
            params:{id : '@id'},
            isArray:true
         },
         active: {method: 'GET',
            url: '/api/installment/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(installment) {

         // naprawic daty
//          installment.createdAt = dateResolveUtil.convertToDate(installment.createdAt);
//         installment.startDate = dateResolveUtil.convertToDate(installment.startDate);
//         installment.finishDate = dateResolveUtil.convertToDate(installment.finishDate);
      }
   }
})();
