(function () {
   'use strict';

   angular
           .module('bank')
           .factory('ActiveDeposit', ActiveDeposit);

   ActiveDeposit.$inject = ['$resource', 'dateResolveUtil'];

   function ActiveDeposit($resource, dateResolveUtil) {
      return $resource('/api/activeDeposit/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/activeDeposit/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         finish : {
            method:'DELETE',
            url: '/api/activeDeposit/finish/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/activeDeposit/delete/:id',
            params:{id : '@id'}
         },
         stop:{
            method:'DELETE',
            url: '/api/activeDeposit/stop/:id',
            params:{id : '@id'}
         },
         active: {method: 'GET',
            url: '/api/activeDeposit/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(activeDeposit) {
         // naprawic daty
//          activeDeposit.createdAt = dateResolveUtil.convertToDate(activeDeposit.createdAt);
//         activeDeposit.startDate = dateResolveUtil.convertToDate(activeDeposit.startDate);
//         activeDeposit.finishDate = dateResolveUtil.convertToDate(activeDeposit.finishDate);
      }
   }
})();
