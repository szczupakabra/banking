(function () {
   'use strict';

   angular
           .module('bank')
           .config(activeDepositProvider);

   activeDepositProvider.$inject = ['$stateProvider'];

   function activeDepositProvider($stateProvider) {
      var resolveDeposits = ['$state', 'Deposit', 'logToServerUtil', loadDeposits];
      var resolveMyAccounts = ['$state', 'Myaccount', 'logToServerUtil', loadMyAccounts];
      var resolveActiveDeposits = ['$state', 'ActiveDeposit', 'logToServerUtil', loadActiveDeposits];
      var resolveSingleActiveDeposit = ['$state', '$stateParams', 'ActiveDeposit', 'logToServerUtil',
         loadSingleActiveDeposit];

      $stateProvider
              .state('activeDeposit', {
                 parent: 'root',
                 url: '/activeDeposit',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('activeDeposit.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/activedeposit/list',
                 controller: 'ActiveDepositListCtrl',
                 resolve: {
                    activeDeposits: resolveActiveDeposits,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeDeposit/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeDeposit.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/activedeposit/add',
                 controller: 'ActiveDepositAddCtrl',
                 resolve: {
                     deposits: resolveDeposits,
                     myAccounts: resolveMyAccounts,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeDeposit/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeDeposit.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/activeDeposit/edit',
                 controller: 'ActiveDepositEditCtrl',
                 resolve: {
                    activeDeposit: resolveSingleActiveDeposit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeDeposit/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeDeposit.common', {
                 url: '/{id:int}',
                 templateUrl: '/activeDeposit/common',
                 controller: 'ActiveDepositCommonCtrl',
                 redirectTo: 'activeDeposit.common.details',
                 resolve: {
                    activeDeposit: resolveSingleActiveDeposit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeDeposit/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('activeDeposit.common.details', {
                 url: '/details',
                 templateUrl: '/activeDeposit/details',
                 controller: 'ActiveDepositDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('activeDeposit/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadActiveDeposits($state, ActiveDeposit, logToServerUtil) {
         var activeDepositsPromise = ActiveDeposit.query().$promise;
         activeDepositsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get ActiveDeposits failed', reason);
//                    $state.go('dashboard');
                 });
         return activeDepositsPromise;
      }

      function loadSingleActiveDeposit($state, $stateParams, ActiveDeposit, logToServerUtil) {
         var activeDepositPromise = ActiveDeposit.get({id: $stateParams.id}).$promise;
         activeDepositPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get ActiveDeposit failed', reason);
                    $state.go('dashboard');
                 });
         return activeDepositPromise;
      }
      function loadDeposits($state, Deposit, logToServerUtil) {
         var depositsPromise = Deposit.query().$promise;
         depositsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Deposits failed', reason);
//                    $state.go('dashboard');
                 });
         return depositsPromise;
      }
      function loadMyAccounts($state, Myaccount, logToServerUtil) {
         var myaccountsPromise = Myaccount.query().$promise;
         myaccountsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Myaccounts failed', reason);
//                    $state.go('dashboard');
                 });
         return myaccountsPromise;
      }
   }
})();
