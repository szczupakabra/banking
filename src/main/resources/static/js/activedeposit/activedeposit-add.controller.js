(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveDepositAddCtrl', ActiveDepositAddCtrl);

   ActiveDepositAddCtrl.$inject = [
      '$scope',
      '$state',
       'myAccounts',
       'deposits',
      'ActiveDeposit',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'languageUtil',
       'popupService'];

   function ActiveDepositAddCtrl($scope, $state, myAccounts, deposits, ActiveDeposit, CommonUtilService, logToServerUtil,dateResolveUtil, languageUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

       $scope.deposits = deposits;

       for(var i=0; i< myAccounts.length; i++) {
           if(myAccounts[i].active) {
               $scope.activeAccount = myAccounts[i];
           }
       }

      $scope.activeDeposit = {
          amount: "0",
          depositId: "-1"
      };
      
      $scope.saveActiveDeposit = saveActiveDeposit;

      function saveActiveDeposit() {
         var activeDeposit = new ActiveDeposit();
         activeDeposit.amount = $scope.activeDeposit.amount;
          activeDeposit.depositId = $scope.activeDeposit.depositId;

        
         activeDeposit.$save()
                 .then(function (result) {
                     popupService.success('activeDeposit-add.popup.buy.success.title','activeDeposit-add.popup.buy.success.body');
                 }, function (reason) {
                    popupService.error( 'activeDeposit-add.popup.buy.failure.title','activeDeposit-add.popup.buy.failure.body');
                 });
      }
   }
})();
