(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveDepositCommonCtrl', ActiveDepositCommonCtrl);

   ActiveDepositCommonCtrl.$inject = ['$scope', 'activeActiveDeposit'];

   function ActiveDepositCommonCtrl($scope, activeActiveDeposit) {
      $scope.activeActiveDeposit = activeActiveDeposit;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
