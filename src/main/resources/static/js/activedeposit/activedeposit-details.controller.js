(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveDepositDetailsCtrl', ActiveDepositDetailsCtrl);

   ActiveDepositDetailsCtrl.$inject = ['$scope', 'activeDeposit'];

   function ActiveDepositDetailsCtrl($scope, activeDeposit) {
      $scope.activeDeposit = activeDeposit;
      $scope.$parent.changeView('DETAILS');
   }
})();
