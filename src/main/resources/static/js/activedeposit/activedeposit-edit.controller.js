(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveDepositEditCtrl', ActiveDepositEditCtrl);

   ActiveDepositEditCtrl.$inject = [
      '$scope',
      '$state',
      'activeDeposit',
      'ActiveDeposit',
      'CommonUtilService',
      'logToServerUtil'];

   function ActiveDepositEditCtrl($scope, $state,activeDeposit, ActiveDeposit, CommonUtilService, logToServerUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.activeDeposit = {
         activeDepositId :activeDeposit.activeDepositId,
         changingRate : activeDeposit.changingRate,
         interest : activeDeposit.interest,
         insurance : activeDeposit.insurance,
         installment : activeDeposit.installment,
         provision : activeDeposit.provision,
         contribution : activeDeposit.contribution,
         maxValue:activeDeposit.maxValue,
         minValue:activeDeposit.minValue
      };
      $scope.saveActiveDeposit = saveActiveDeposit;

      function saveActiveDeposit() {
         var activeDeposit = new ActiveDeposit();
         activeDeposit.changingRate = $scope.activeDeposit.changingRate;
         activeDeposit.interest = $scope.activeDeposit.interest;
         activeDeposit.insurance = $scope.activeDeposit.insurance;
         activeDeposit.installment = $scope.activeDeposit.installment;
         activeDeposit.provision = $scope.activeDeposit.provision;
         activeDeposit.contribution = $scope.activeDeposit.contribution;
         activeDeposit.maxValue = $scope.activeDeposit.maxValue;
         activeDeposit.minValue = $scope.activeDeposit.minValue;
         activeDeposit.activeDepositId = $scope.activeDeposit.activeDepositId;
         
         activeDeposit.$update()
                 .then(function (result) {
                    $state.go('activeDeposit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();