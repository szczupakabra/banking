(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveDepositListCtrl', ActiveDepositListCtrl);

   ActiveDepositListCtrl.$inject = [
      '$scope',
      'ActiveDeposit',
      'activeDeposits',
      '$stateParams',
      '$location',
      '$timeout'];

   function ActiveDepositListCtrl($scope, ActiveDeposit, activeDeposits, $stateParams, $location, $timeout) {
      $scope.loading = false;
       activeDeposits.map(function(entry) {
           if(entry.constant == 4) {
               entry.constant = 'Years';
           } else if(entry.constant == 2) {
               entry.constant = 'Months';
           } else if(entry.constant == 1) {
               entry.constant = 'Days';
           }
           return entry;
       });


      $scope.activeDeposits = activeDeposits;

      $scope.refresh = refresh;
      $scope.logAction = logAction;
      $scope.finishActiveDeposit = finishActiveDeposit;
      $scope.deleteActiveDeposit = deleteActiveDeposit;
      $scope.stopActiveDeposit = stopActiveDeposit


      function reloadActiveDeposits() {
         $scope.loading = true;
         ActiveDeposit.query().$promise
                 .then(function (data) {
                    $scope.activeDeposits = data;
                    $scope.loading = false;
                 });
      }

      function logAction(whonUser, actionName) {
         $scope.loading = true;
         var action = new ActiveDeposit();
         action.whonUser = whonUser;
         action.actionName = actionName;
         action.$logAction()
                 .then(function (data) {
                    $scope.loading = false;
                 });
      }


      function deleteActiveDeposit(activeDepositId) {
         $scope.loading = true;
         ActiveDeposit.delete({id: activeDepositId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.activeDeposits.length; i++) {
                       if ($scope.activeDeposits[i].activeDepositId === activeDepositId) {
                          $scope.activeDeposits.splice(i, 1);
                       }
                    }
                 });
      }

      function stopActiveDeposit(activeDepositId) {
         $scope.loading = true;
         ActiveDeposit.stop({id: activeDepositId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.activeDeposits.length; i++) {
                       if ($scope.activeDeposits[i].activeDepositId === activeDepositId) {
                          $scope.activeDeposits.splice(i, 1);
                       }
                    }
                 });
      }

      function finishActiveDeposit(activeDepositId) {
         $scope.loading = true;
         ActiveDeposit.finish({id: activeDepositId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.activeDeposits.length; i++) {
                       if ($scope.activeDeposits[i].activeDepositId === activeDepositId) {
                          $scope.activeDeposits[i].active = true;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function refresh() {
         ActiveDeposit.refresh().$promise
                 .then(function () {
                    reloadActiveDeposits();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();
