(function () {
   'use strict';

   angular
           .module('bank')
           .config(myaccountProvider);

   myaccountProvider.$inject = ['$stateProvider'];

   function myaccountProvider($stateProvider) {
      var resolveMyaccounts = ['$state', 'Myaccount', 'logToServerUtil', loadMyaccounts];
      var resolveCurrencys = ['$state', 'Currency', 'logToServerUtil', loadCurrencys];
      var resolveSingleMyaccount = ['$state', '$stateParams', 'Myaccount', 'logToServerUtil',
         loadSingleMyaccount];

      $stateProvider
              .state('myaccount', {
                 parent: 'root',
                 url: '/myaccount',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('myaccount.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/myaccount/list',
                 controller: 'MyaccountListCtrl',
                 resolve: {
                    myaccounts: resolveMyaccounts,
                    currencys: resolveCurrencys,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('myaccount.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/myaccount/add',
                 controller: 'MyaccountAddCtrl',
                 resolve: {
                    currencys: resolveCurrencys,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('myaccount.userAdd', {
                 url: '/add/{userId:int}',
                 reloadOnSearch: false,
                 templateUrl: '/myaccount/add',
                 controller: 'MyaccountAddCtrl',
                 resolve: {
                    currencys: resolveCurrencys,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/add');
                          return $translate.refresh();
                       }]
                 }
              })
               .state('myaccount.boost', {
                 url: '/boost/{id:int}/{userId:int}',
                 reloadOnSearch: false,
                 templateUrl: '/myaccount/boost',
                 controller: 'MyaccountBoostCtrl',
                 resolve: {
                    myaccount: resolveSingleMyaccount,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/boost');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('myaccount.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/myaccount/edit',
                 controller: 'MyaccountEditCtrl',
                 resolve: {
//                    myaccount: resolveSingleMyaccount,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('myaccount.common', {
                 url: '/{id:int}',
                 templateUrl: '/myaccount/common',
                 controller: 'MyaccountCommonCtrl',
                 redirectTo: 'myaccount.common.details',
                 resolve: {
//                    myaccount: resolveSingleMyaccount,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('myaccount.common.details', {
                 url: '/details',
                 templateUrl: '/myaccount/details',
                 controller: 'MyaccountDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('myaccount/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadMyaccounts($state, Myaccount, logToServerUtil) {
         var myaccountsPromise = Myaccount.query().$promise;
         myaccountsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Myaccounts failed', reason);
//                    $state.go('dashboard');
                 });
         return myaccountsPromise;
      }
      
      function loadCurrencys($state, Currency, logToServerUtil) {
         var currencyPromise = Currency.query().$promise;
         currencyPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Myaccounts failed', reason);
//                    $state.go('dashboard');
                 });
         return currencyPromise;
      }

      function loadSingleMyaccount($state, $stateParams, Myaccount, logToServerUtil) {
         var myaccountPromise = Myaccount.get({id: $stateParams.id}).$promise;
         myaccountPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Myaccount failed', reason);
                    $state.go('dashboard');
                 });
         return myaccountPromise;
      }
   }
})();
