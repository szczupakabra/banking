(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountDetailsCtrl', MyaccountDetailsCtrl);

   MyaccountDetailsCtrl.$inject = ['$scope'/*, 'myaccount'*/];

   function MyaccountDetailsCtrl($scope/*, myaccount*/) {
//      $scope.myaccount = myaccount;
      $scope.$parent.changeView('DETAILS');
   }
})();
