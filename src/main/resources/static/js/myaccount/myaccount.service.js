(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Myaccount', Myaccount);

   Myaccount.$inject = ['$resource', 'dateResolveUtil'];

   function Myaccount($resource, dateResolveUtil) {
      return $resource('/api/account/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/account/refresh',
            isArray: false
         },
         payment: {
            method: 'POST',
            url: '/api/account/payment',
            isArray: false
         },
         active : {
            method:'PUT',
            url: '/api/account/active/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/account/delete/:id',
            params:{id : '@id'}
         },
         userList: {method: 'GET',
            url: '/api/account/user/:userId',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(myaccount) {

         // naprawic daty
//          myaccount.createdAt = dateResolveUtil.convertToDate(myaccount.createdAt);
//         myaccount.startDate = dateResolveUtil.convertToDate(myaccount.startDate);
//         myaccount.finishDate = dateResolveUtil.convertToDate(myaccount.finishDate);
      }
   }
})();
