(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountListCtrl', MyaccountListCtrl);

   MyaccountListCtrl.$inject = [
      '$scope',
      'Myaccount',
      'myaccounts',
      'currencys',
      '$stateParams',
      '$location',
      'popupService'];

   function MyaccountListCtrl($scope, Myaccount, myaccounts, currencys, $stateParams, $location, popupService) {
      $scope.loading = false;
      $scope.myaccounts = myaccounts;

      for (var i = 0; i < $scope.myaccounts.length; i++) {
         $scope.myaccounts[i].currency = _findCurrency($scope.myaccounts[i]);
      }

      $scope.refresh = refresh;
      $scope.activeMyaccount = activeMyaccount;
      $scope.deleteMyaccount = deleteMyaccount;

      function _findCurrency(account) {
         for (var i = 0; i < currencys.length; i++) {
            if (currencys[i].currencyId === account.currencyId) {
               return currencys[i].symbol;
            }
         }
      }


      function reloadMyaccounts() {
         $scope.loading = true;
         Myaccount.query().$promise
                 .then(function (data) {
                    $scope.myaccounts = data;
                    $scope.loading = false;
                 });
      }



      function deleteMyaccount(myaccountId) {
         $scope.loading = true;
         Myaccount.delete({id: myaccountId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('myaccount-list.popup.delete.success.title', 'myaccount-list.popup.delete.success.body');
                    for (var i = 0; i < $scope.myaccounts.length; i++) {
                       if ($scope.myaccounts[i].myaccountId === myaccountId) {
                          $scope.myaccounts.splice(i, 1);
                       }
                    }
                 });
      }

      function activeMyaccount(myaccountId) {
         $scope.loading = true;
         Myaccount.active({id: myaccountId}).$promise
                 .then(function (data) {
                    popupService.success('myaccount-list.popup.active.success.title', 'myaccount-list.popup.active.success.body');
                    for (var i = 0; i < $scope.myaccounts.length; i++) {
                       if ($scope.myaccounts[i].accountId === myaccountId) {
                          $scope.myaccounts[i].active = true;
                       } else {
                          $scope.myaccounts[i].active = false;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function refresh() {
         Myaccount.refresh().$promise
                 .then(function () {
                    reloadMyaccounts();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();