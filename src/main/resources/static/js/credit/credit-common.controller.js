(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditCommonCtrl', CreditCommonCtrl);

   CreditCommonCtrl.$inject = ['$scope', 'credit'];

   function CreditCommonCtrl($scope, credit) {
      $scope.credit = credit;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
