(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CurrencyEditCtrl', CurrencyEditCtrl);

   CurrencyEditCtrl.$inject = [
      '$scope',
      '$state',
      'currency',
      'Currency',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function CurrencyEditCtrl($scope, $state, currency, Currency, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.currency = {
         currencyId: currency.currencyId,
         symbol: currency.symbol
      };
      $scope.saveCurrency = saveCurrency;

      function saveCurrency() {
         var currency = new Currency();
         currency.symbol = $scope.currency.symbol;
         currency.currencyId = $scope.currency.currencyId;
         currency.$update()
                 .then(function (result) {
                    popupService.success('currency-edit.popup.save.success.title', 'currency-edit.popup.save.success.body');
                    $state.go('currency.list');
                 }, function (reason) {
                    popupService.error('currency-edit.popup.save.failure.title', 'currency-edit.popup.save.failure.body');
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();