(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserCommonCtrl', UserCommonCtrl);

   UserCommonCtrl.$inject = ['$scope', 'user'];

   function UserCommonCtrl($scope, user) {
      $scope.user = user;
      
   }
})();
