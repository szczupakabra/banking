(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserEditCtrl', UserEditCtrl);

   UserEditCtrl.$inject = [
      '$scope',
      '$state',
      'user',
      'User',
      'CommonUtilService',
      'logToServerUtil',
      'popupService'];

   function UserEditCtrl($scope, $state, user, User, CommonUtilService, logToServerUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.user = {
         firstname: user.firstname,
         lastname: user.lastname,
         userId: user.userId
      };
      $scope.saveUser = saveUser;

      function saveUser() {
         var user = new User();
         user.firstname = $scope.user.firstname;
         user.lastname = $scope.user.lastname;
         user.userId = $scope.user.userId;
         user.$update()
                 .then(function (result) {
                    popupService.success('user-edit.popup.save.success.title', 'user-edit.popup.save.success.body');
                    $state.go('user.list');
                 }, function (reason) {
                    popupService.error('user-edit.popup.save.failure.title', 'user-edit.popup.save.failure.body');
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();