(function () {
   'use strict';

   angular
           .module('bank')
           .controller('DepositListCtrl', DepositListCtrl);

   DepositListCtrl.$inject = [
      '$scope',
       'currencies',
      'Deposit',
      'deposits',
      '$stateParams',
      '$location',
      '$timeout'];

   function DepositListCtrl($scope, currencies, Deposit, deposits, $stateParams, $location, $timeout) {
      $scope.loading = false;
       deposits.map(function(entry) {
           if(entry.constant == 3) {
               entry.constant = 'Lata';
           } else if(entry.constant == 2) {
               entry.constant = 'Miesiące';
           } else if(entry.constant == 1) {
               entry.constant = 'Dni';
           }
           return entry;
       });

       deposits.map(function(entry) {
           entry.currencySymbol = 'ERR';
           for (var i=0; i<currencies.length; i++) {
               if(currencies[i].currencyId === entry.currencyId) {
                   entry.currencySymbol = currencies[i].symbol;
               }
           }
           return entry;
       });


      $scope.deposits = deposits;

      $scope.refresh = refresh;
      $scope.logAction = logAction;
      $scope.finishDeposit = finishDeposit;
      $scope.deleteDeposit = deleteDeposit;


      function reloadDeposits() {
         $scope.loading = true;
         Deposit.query().$promise
                 .then(function (data) {
                    $scope.deposits = data;
                    $scope.loading = false;
                 });
      }

      function logAction(whonUser, actionName) {
         $scope.loading = true;
         var action = new Deposit();
         action.whonUser = whonUser;
         action.actionName = actionName;
         action.$logAction()
                 .then(function (data) {
                    $scope.loading = false;
                 });
      }


      function deleteDeposit(depositId) {
         $scope.loading = true;
         Deposit.delete({id: depositId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.deposits.length; i++) {
                       if ($scope.deposits[i].depositId === depositId) {
                          $scope.deposits.splice(i, 1);
                       }
                    }
                 });
      }

      function finishDeposit(depositId) {
         $scope.loading = true;
         Deposit.finish({id: depositId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.deposits.length; i++) {
                       if ($scope.deposits[i].depositId === depositId) {
                          $scope.deposits[i].active = true;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function refresh() {
         Deposit.refresh().$promise
                 .then(function () {
                    reloadDeposits();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();
