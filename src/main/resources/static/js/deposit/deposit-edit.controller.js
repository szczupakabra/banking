(function () {
   'use strict';

   angular
           .module('bank')
           .controller('DepositEditCtrl', DepositEditCtrl);

   DepositEditCtrl.$inject = [
      '$scope',
      '$state',
      'deposit',
      'Deposit',
      'CommonUtilService',
      'logToServerUtil'];

   function DepositEditCtrl($scope, $state,deposit, Deposit, CommonUtilService, logToServerUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.deposit = {
         depositId :deposit.depositId,
         changingRate : deposit.changingRate,
         interest : deposit.interest,
         insurance : deposit.insurance,
         installment : deposit.installment,
         provision : deposit.provision,
         contribution : deposit.contribution,
         maxValue:deposit.maxValue,
         minValue:deposit.minValue
      };
      $scope.saveDeposit = saveDeposit;

      function saveDeposit() {
         var deposit = new Deposit();
         deposit.changingRate = $scope.deposit.changingRate;
         deposit.interest = $scope.deposit.interest;
         deposit.insurance = $scope.deposit.insurance;
         deposit.installment = $scope.deposit.installment;
         deposit.provision = $scope.deposit.provision;
         deposit.contribution = $scope.deposit.contribution;
         deposit.maxValue = $scope.deposit.maxValue;
         deposit.minValue = $scope.deposit.minValue;
         deposit.depositId = $scope.deposit.depositId;
         
         deposit.$update()
                 .then(function (result) {
                    $state.go('deposit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();