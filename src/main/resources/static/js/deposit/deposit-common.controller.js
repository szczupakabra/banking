(function () {
   'use strict';

   angular
           .module('bank')
           .controller('DepositCommonCtrl', DepositCommonCtrl);

   DepositCommonCtrl.$inject = ['$scope', 'deposit'];

   function DepositCommonCtrl($scope, deposit) {
      $scope.deposit = deposit;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
