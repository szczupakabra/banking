(function () {
   'use strict';

   angular
           .module('bank')
           .controller('DepositAddCtrl', DepositAddCtrl);

   DepositAddCtrl.$inject = [
      '$scope',
      '$state',
       'currencies',
      'Deposit',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'languageUtil'];

   function DepositAddCtrl($scope, $state, currencies, Deposit, CommonUtilService, logToServerUtil,dateResolveUtil, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

       $scope.currencies = currencies;

      $scope.deposit = {
         constant: '',
         rate: '',
         minCash: '',
         maxCash : '',
         renewable : false,
         capitalizationPeriod : '',
         breakPercentage : '0',
         currencyId : '0',
         duration: '0'
      };
      
      $scope.saveDeposit = saveDeposit;

      function saveDeposit() {
         var deposit = new Deposit();
         deposit.rate = $scope.deposit.rate;
         deposit.minCash = $scope.deposit.minCash;
         deposit.maxCash = $scope.deposit.maxCash;
         deposit.renewable = $scope.deposit.renewable;
          if($scope.deposit.capitalizationPeriodType === 'day') {
              deposit.constant = 1;
          } else if ($scope.deposit.capitalizationPeriodType === 'month') {
              deposit.constant = 2;
          } else if ($scope.deposit.capitalizationPeriodType === 'year') {
              deposit.constant = 3;
          }
         deposit.capitalizationPeriod = $scope.deposit.capitalizationPeriodAmount;
         deposit.breakPercentage = $scope.deposit.breakPercentage;
          deposit.currencyId = $scope.deposit.currencyId;
          deposit.duration = $scope.deposit.duration;

        
         deposit.$save()
                 .then(function (result) {
                    $state.go('deposit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Deposit failure', reason);
                 });
      }
   }
})();
