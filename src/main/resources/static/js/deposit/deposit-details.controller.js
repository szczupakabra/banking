(function () {
   'use strict';

   angular
           .module('bank')
           .controller('DepositDetailsCtrl', DepositDetailsCtrl);

   DepositDetailsCtrl.$inject = ['$scope', 'deposit'];

   function DepositDetailsCtrl($scope, deposit) {
      $scope.deposit = deposit;
      $scope.$parent.changeView('DETAILS');
   }
})();
