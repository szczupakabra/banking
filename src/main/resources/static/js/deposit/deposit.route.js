(function () {
   'use strict';

   angular
           .module('bank')
           .config(depositProvider);

   depositProvider.$inject = ['$stateProvider'];

   function depositProvider($stateProvider) {
      var resolveDeposits = ['$state', 'Deposit', 'logToServerUtil', loadDeposits];
      var resolveCurrencies = ['$state', 'Currency', 'logToServerUtil', loadCurrencies];
      var resolveSingleDeposit = ['$state', '$stateParams', 'Deposit', 'logToServerUtil',
         loadSingleDeposit];

      $stateProvider
              .state('deposit', {
                 parent: 'root',
                 url: '/deposit',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('deposit.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/deposit/list',
                 controller: 'DepositListCtrl',
                 resolve: {
                    deposits: resolveDeposits,
                    currencies: resolveCurrencies,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('deposit/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('deposit.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/deposit/add',
                 controller: 'DepositAddCtrl',
                 resolve: {
                    currencies: resolveCurrencies,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('deposit/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('deposit.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/deposit/edit',
                 controller: 'DepositEditCtrl',
                 resolve: {
                    currencies: resolveCurrencies,
                    deposit: resolveSingleDeposit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('deposit/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('deposit.common', {
                 url: '/{id:int}',
                 templateUrl: '/deposit/common',
                 controller: 'DepositCommonCtrl',
                 redirectTo: 'deposit.common.details',
                 resolve: {
                    deposit: resolveSingleDeposit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('deposit/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('deposit.common.details', {
                 url: '/details',
                 templateUrl: '/deposit/details',
                 controller: 'DepositDetailsCtrl',
                 resolve: {
                    currencies: resolveCurrencies,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('deposit/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadDeposits($state, Deposit, logToServerUtil) {
         var depositsPromise = Deposit.query().$promise;
         depositsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Deposits failed', reason);
//                    $state.go('dashboard');
                 });
         return depositsPromise;
      }
       
      function loadCurrencies($state, Currency, logToServerUtil) {
         var currencyPromise = Currency.query().$promise;
         currencyPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Currency failed', reason);
//                    $state.go('dashboard');
                 });
         return currencyPromise;
      }

      function loadSingleDeposit($state, $stateParams, Deposit, logToServerUtil) {
         var depositPromise = Deposit.get({id: $stateParams.id}).$promise;
         depositPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Deposit failed', reason);
                    $state.go('dashboard');
                 });
         return depositPromise;
      }
   }
})();
