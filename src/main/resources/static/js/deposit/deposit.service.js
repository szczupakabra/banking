(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Deposit', Deposit);

   Deposit.$inject = ['$resource', 'dateResolveUtil'];

   function Deposit($resource, dateResolveUtil) {
      return $resource('/api/deposit/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/deposit/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         finish : {
            method:'DELETE',
            url: '/api/deposit/finish/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/deposit/delete/:id',
            params:{id : '@id'}
         },
         active: {method: 'GET',
            url: '/api/deposit/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(deposit) {

         // naprawic daty
//          deposit.createdAt = dateResolveUtil.convertToDate(deposit.createdAt);
//         deposit.startDate = dateResolveUtil.convertToDate(deposit.startDate);
//         deposit.finishDate = dateResolveUtil.convertToDate(deposit.finishDate);
      }
   }
})();
